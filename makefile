GITPATH=gitlab.com/ilmikko/overwatch

.PHONY: test

PACKAGES=./config\
				 ./config/parser\
				 ./overwatch\
				 ./overwatch/bufio/demuxer\
				 ./overwatch/bufio/writer_closer\
				 ./overwatch/filter\
				 ./overwatch/io/demuxer\
				 ./overwatch/io/null\
				 ./overwatch/io/threaded\
				 ./overwatch/str\
				 ./overwatch/unit\

# ENFORCE VIRTUAL GOPATH ENVIRONMENT FOR MANIFESTATION PURPOSES
build:
	mkdir -p /tmp/go/src/$(shell dirname $(GITPATH))
	test -L /tmp/go/src/$(GITPATH) || ln -sf $(shell pwd) /tmp/go/src/$(GITPATH)
	GOPATH="/tmp/go" go build -o bin/overwatch overwatch.go
	@rm -rf /tmp/go

test-golden-save:
	cd golden && ./golden.sh --save

test-golden:
	cd golden && ./golden.sh

test-unit:
	go test $(PACKAGES)

test: test-unit test-golden
