package main

import (
	"fmt"
	"os"

	"gitlab.com/ilmikko/overwatch/comm"
	"gitlab.com/ilmikko/overwatch/config"
	"gitlab.com/ilmikko/overwatch/overwatch"
)

func mainErr(co *comm.Comm) error {
	cfg, err := config.Load(co)
	if err != nil {
		return fmt.Errorf("CONFIGURATION MISMATCH.\n%v", err)
	}

	o, err := overwatch.New(cfg)
	if err != nil {
		return err
	}
	defer o.Close()

	if err := o.Watch(); err != nil {
		return err
	}

	return nil
}

func main() {
	co := comm.New()
	if err := mainErr(co); err != nil {
		co.Printf("ALERT. TERMINATION IMMINENT: %s", err)
		os.Exit(1)
	}
}
