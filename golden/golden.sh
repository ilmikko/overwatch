#!/usr/bin/env bash

SAVE=0;

case $@ in
	--save)
		SAVE=1;
		;;
esac

for folder in *; do
	[ -d "$folder" ] || continue;
	(
	echo "$folder";
	cd "$folder";
	(
	rm -rf "./got";
	mkdir -p "./got";
	cd "./got";
	if [ -f "../STDIN.txt" ]; then
		cat ../STDIN.txt | go run ../../../overwatch.go --time=false --config ../test.conf > STDOUT.txt 2> STDERR.txt || exit 1;
	elif [ -f "../STDIN.sh" ]; then
		../STDIN.sh | go run ../../../overwatch.go --time=false --config ../test.conf > STDOUT.txt 2> STDERR.txt || exit 1;
	else
		go run ../../../overwatch.go --time=false --config ../test.conf > STDOUT.txt 2> STDERR.txt || exit 1;
	fi
)
if [ "$SAVE" = "1" ]; then
	cp -rv "./got/"* "./want/";
else
	diff --recursive got want || exit 1;
fi
rm -rf "./got";
)
done
