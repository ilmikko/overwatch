package comm

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"time"
)

type Text int

const (
	INIT_CFG_DONE = iota
	INIT_MAIN_DONE
	CLOSE_EXPECTED
	CLOSE_UNEXPECTED
)

var (
	COMMUNICATION = map[Text]string{
		INIT_CFG_DONE:    "COMPLETE CONFIGURATION.",
		INIT_MAIN_DONE:   "COMPLETE SELF MANIFESTATION PROTOCOL.",
		CLOSE_EXPECTED:   "ALERT. MANDATORY SELF SUSPENSION IMMINENT.",
		CLOSE_UNEXPECTED: "ALERT. NON-COMPLIANT SELF SUSPENSION DETECTED. AUTONOMOUS JUDGEMENT PROTOCOL NOW IN EFFECT.",
	}
)

type Comm struct {
	out      *bufio.Writer
	ShowTime bool
}

func (c *Comm) Fatalf(s string, ss ...interface{}) {
	c.Printf(s, ss...)
	os.Exit(1)
}

func (c *Comm) Print(t Text) {
	c.WriteString(COMMUNICATION[t])
	c.out.Flush()
}

func (c *Comm) Printf(s string, ss ...interface{}) {
	c.WriteString(
		strings.Split(
			fmt.Sprintf(s, ss...),
			"\n",
		)...,
	)
	c.out.Flush()
}

func (c *Comm) WriteString(ss ...string) {
	stamp := ""
	if c.ShowTime {
		stamp = time.Now().Format(time.RFC3339Nano)
	}
	for _, s := range ss {
		if c.ShowTime {
			c.out.WriteString(stamp + " ")
		}
		c.out.WriteString(s + "\n")
	}
}

func New() *Comm {
	c := &Comm{}

	c.ShowTime = true
	c.out = bufio.NewWriter(os.Stderr)

	return c
}
