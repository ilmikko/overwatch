package unit

import (
	"io"

	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Mux struct {
	Base
	readerQueue []str.Reader
}

func (u *Mux) nextReader() {
	u.reader, u.readerQueue = u.readerQueue[0], u.readerQueue[1:]
}

func (u *Mux) ReadString(b byte) (string, error) {
	for {
		str, err := u.reader.ReadString(b)
		if err != nil {
			if err == io.EOF {
				if len(u.readerQueue) == 0 {
					break
				}
				u.nextReader()
				continue
			}
			return "", err
		}
		return str, nil
	}
	return "", io.EOF
}

func NewMux(rs ...str.Reader) Unit {
	u := &Mux{}

	u.readerQueue = rs
	u.nextReader()

	return u
}
