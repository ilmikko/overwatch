package unit

import (
	"strconv"

	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Count struct {
	Base
	counts map[string]int
}

func (u *Count) ReadString(b byte) (string, error) {
	str, err := u.reader.ReadString(b)
	if err != nil {
		return "", err
	}

	c, ok := u.counts[str]
	if !ok {
		c = 0
	}
	c++
	u.counts[str] = c

	return strconv.Itoa(c) + " " + str, nil
}

func NewCount(r str.Reader) Unit {
	u := &Count{}

	u.counts = map[string]int{}
	u.reader = r

	return u
}
