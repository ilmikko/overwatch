package unit

import (
	"io"

	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Limit struct {
	Base
	limit int
}

// Only read N strings of the stream before EOF.
func (u *Limit) ReadString(b byte) (string, error) {
	if u.limit--; u.limit < 0 {
		return "", io.EOF
	}
	str, err := u.reader.ReadString(b)
	if err != nil {
		return "", err
	}
	return str, nil
}

func NewLimit(n int, r str.Reader) Unit {
	u := &Limit{}

	u.reader = r
	u.limit = n

	return u
}

type LimitNegated struct {
	Limit
}

// Discard first N strings of the stream.
func (u *LimitNegated) ReadString(b byte) (string, error) {
	for {
		if u.limit--; u.limit < 0 {
			break
		}
		_, err := u.reader.ReadString(b)
		if err != nil {
			return "", err
		}
	}
	return u.reader.ReadString(b)
}

func NewLimitNegated(n int, r str.Reader) Unit {
	u := &LimitNegated{}

	u.reader = r
	u.limit = n

	return u
}
