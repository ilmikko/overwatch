package unit

import (
	"strings"

	"gitlab.com/ilmikko/overwatch/overwatch/filter"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Match struct {
	Base

	filter *filter.Filter
}

func (u *Match) ReadString(b byte) (string, error) {
	for {
		str, err := u.reader.ReadString(b)
		if err != nil {
			return "", err
		}

		if u.filter.Matches(
			strings.TrimSuffix(str, "\n"),
		) {
			return str, nil
		}
	}
}

func NewMatch(f *filter.Filter, r str.Reader) Unit {
	u := &Match{}

	u.reader = r
	u.filter = f

	return u
}

type MatchNegated struct {
	Match
}

func (u *MatchNegated) ReadString(b byte) (string, error) {
	for {
		str, err := u.reader.ReadString(b)
		if err != nil {
			return "", err
		}

		if !u.filter.Matches(
			strings.TrimSuffix(str, "\n"),
		) {
			return str, nil
		}
	}
}

func NewMatchNegated(f *filter.Filter, r str.Reader) Unit {
	u := &MatchNegated{}

	u.reader = r
	u.filter = f

	return u
}
