package unit

import (
	"gitlab.com/ilmikko/overwatch/overwatch/filter"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Replace struct {
	Base

	filter      *filter.Filter
	replacement string
}

func (u *Replace) ReadString(b byte) (string, error) {
	for {
		str, err := u.reader.ReadString(b)
		if err != nil {
			return "", err
		}

		return u.filter.Replace(
			str,
			u.replacement,
		), nil
	}
}

func NewReplace(f *filter.Filter, repl string, r str.Reader) Unit {
	u := &Replace{}

	u.replacement = repl
	u.reader = r
	u.filter = f

	return u
}
