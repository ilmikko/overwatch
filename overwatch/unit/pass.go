package unit

import (
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Pass struct {
	Base
}

func (u *Pass) ReadString(b byte) (string, error) {
	str, err := u.reader.ReadString(b)
	if err != nil {
		return "", err
	}
	return str, nil
}

func NewPass(r str.Reader) Unit {
	u := &Pass{}

	u.reader = r

	return u
}
