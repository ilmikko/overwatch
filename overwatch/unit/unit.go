package unit

import "gitlab.com/ilmikko/overwatch/overwatch/str"

type Unit interface {
	str.Reader
}

type Base struct {
	reader str.Reader
}
