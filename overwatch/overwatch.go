package overwatch

import (
	"sync"

	"gitlab.com/ilmikko/overwatch/comm"
	"gitlab.com/ilmikko/overwatch/config"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
)

type Overwatch struct {
	CFG *config.Config

	WatchUnits []func() error
}

func (o *Overwatch) Close() {
	o.CFG.Comm.Print(comm.CLOSE_EXPECTED)
	// o.CFG.Comm.Print(comm.CLOSE_UNEXPECTED)
}

func (o *Overwatch) Watch() error {
	wg := sync.WaitGroup{}
	wg.Add(len(o.CFG.Drains))
	for _, d := range o.CFG.Drains {
		go func(d pipe.Drain) {
			defer wg.Done()
			if err := d.Flow(); err != nil {
				o.CFG.Comm.Fatalf("FLOW ERROR: %v", err)
			}
			if err := d.Close(); err != nil {
				o.CFG.Comm.Fatalf("CLOSE ERROR: %v", err)
			}
		}(d)
	}
	wg.Wait()
	return nil
}

func New(cfg *config.Config) (*Overwatch, error) {
	o := &Overwatch{}

	o.CFG = cfg

	o.CFG.Comm.Print(comm.INIT_MAIN_DONE)
	return o, nil
}
