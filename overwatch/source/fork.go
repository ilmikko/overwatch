package source

import (
	"bufio"
	"bytes"
	"log"
	"os/exec"
	"strings"

	"gitlab.com/ilmikko/overwatch/overwatch/io/demuxer"
	"gitlab.com/ilmikko/overwatch/overwatch/io/threaded"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Fork struct {
	cmd *exec.Cmd

	stdoutClone *demuxer.Demuxer
}

func (s *Fork) GetOutflow() str.Reader {
	return bufio.NewReader(s.stdoutClone.NewBuffer())
}

// TODO: Use pipe fork?
func NewFork(cl string) (pipe.Source, error) {
	s := &Fork{}

	fields := strings.Fields(cl)
	s.cmd = exec.Command(fields[0], fields[1:]...)

	stderr := &bytes.Buffer{}
	s.cmd.Stderr = stderr

	stdout := threaded.NewBuffer()
	s.cmd.Stdout = stdout

	s.stdoutClone = demuxer.New(stdout)

	go func() {
		if err := s.cmd.Run(); err != nil {
			log.Fatalf("FORK SOURCE: %v: %s", err, stderr.String())
		}
		stdout.EOF()
	}()

	return s, nil
}
