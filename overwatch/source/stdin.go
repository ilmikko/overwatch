package source

import (
	"bufio"
	"os"

	"gitlab.com/ilmikko/overwatch/overwatch/io/demuxer"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

var CloneStdin = demuxer.New(os.Stdin)

type Stdin struct{}

func (s *Stdin) GetOutflow() str.Reader {
	return bufio.NewReader(CloneStdin.NewBuffer())
}

func NewStdin() pipe.Source {
	return &Stdin{}
}
