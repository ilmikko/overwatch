package source

import (
	"gitlab.com/ilmikko/overwatch/overwatch/io/wait"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Wait struct{}

func (s *Wait) GetOutflow() str.Reader {
	return &wait.Reader{}
}

func NewWait() pipe.Source {
	return &Wait{}
}
