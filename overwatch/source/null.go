package source

import (
	"gitlab.com/ilmikko/overwatch/overwatch/io/null"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Null struct{}

func (s *Null) GetOutflow() str.Reader {
	return &null.Reader{}
}

func NewNull() pipe.Source {
	return &Null{}
}
