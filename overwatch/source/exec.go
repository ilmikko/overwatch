package source

import (
	"bufio"
	"log"
	"os/exec"
	"strings"

	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Exec struct {
	name string
	args []string
}

func (s *Exec) GetOutflow() str.Reader {
	cmd := exec.Command(s.name, s.args...)

	r, err := cmd.StdoutPipe()
	if err != nil {
		log.Fatalf("Exec: %v", err)
	}
	if err := cmd.Start(); err != nil {
		log.Fatalf("Exec: %v", err)
	}

	return bufio.NewReader(r)
}

func NewExec(cl string) (pipe.Source, error) {
	s := &Exec{}

	fields := strings.Fields(cl)
	s.name = fields[0]
	s.args = fields[1:]

	return s, nil
}
