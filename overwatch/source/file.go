package source

import (
	"bufio"
	"log"
	"os"

	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type File struct {
	path string
}

func (s *File) GetOutflow() str.Reader {
	f, err := os.Open(s.path)
	if err != nil {
		log.Fatalf("File: %v", err)
	}
	return bufio.NewReader(f)
}

func NewFile(path string) (pipe.Source, error) {
	s := &File{}

	s.path = path

	f, err := os.Open(s.path)
	if err != nil {
		return nil, err
	}
	if err := f.Close(); err != nil {
		return nil, err
	}

	return s, nil
}
