package str

type Reader interface {
	ReadString(byte) (string, error)
}

type Writer interface {
	WriteString(string) (int, error)
	Close() error
	Flush() error
}

type ReadWriter interface {
	Reader
	Writer
}

type LockWriter interface {
	Writer

	Lock()
	Unlock()
}
