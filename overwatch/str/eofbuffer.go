package str

import (
	"bufio"
	"io"
)

type EOFBufferable interface {
	io.Reader
	io.Writer

	Empty() bool
	EOF()
}

type EOFBuffer struct {
	*bufio.ReadWriter
	buf EOFBufferable
}

func (eb *EOFBuffer) Empty() bool {
	return eb.buf.Empty()
}

func (eb *EOFBuffer) EOF() {
	eb.buf.EOF()
}

func NewEOFBuffer(buf EOFBufferable) *EOFBuffer {
	eb := &EOFBuffer{bufio.NewReadWriter(
		bufio.NewReader(buf),
		bufio.NewWriter(buf),
	), buf}

	return eb
}
