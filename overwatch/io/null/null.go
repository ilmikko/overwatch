package null

import "io"

type Reader struct{}

func (r Reader) ReadString(b byte) (string, error) {
	return "", io.EOF
}

func (r Reader) Read([]byte) (int, error) {
	return 0, io.EOF
}

type Writer struct{}

func (w Writer) Close() error {
	return nil
}

func (w Writer) Flush() error {
	return nil
}

func (w Writer) Lock() {}

func (w Writer) Unlock() {}

func (w Writer) WriteString(s string) (int, error) {
	return w.Write([]byte(s))
}

func (w Writer) Write(b []byte) (int, error) {
	return len(b), nil
}

type ReadWriter struct {
	Reader
	Writer
}
