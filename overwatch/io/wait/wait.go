package wait

type Reader struct{}

func (r Reader) ReadString(b byte) (string, error) {
	select {}
}

func (r Reader) Read([]byte) (int, error) {
	select {}
}

type Writer struct{}

func (w Writer) Close() error {
	return nil
}

func (w Writer) Flush() error {
	return nil
}

func (w Writer) Lock() {}

func (w Writer) Unlock() {}

func (w Writer) WriteString(s string) (int, error) {
	select {}
}

func (w Writer) Write(b []byte) (int, error) {
	select {}
}

type ReadWriter struct {
	Reader
	Writer
}
