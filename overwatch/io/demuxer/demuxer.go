package demuxer

import (
	"io"

	"gitlab.com/ilmikko/overwatch/overwatch/io/threaded"
)

type Demuxer struct {
	Clones []*Buffer
	IsEOF  bool

	source      io.Reader
	sourceMutex chan struct{}
}

type Buffer struct {
	cb  *Demuxer
	buf *threaded.Buffer
}

func (s *Buffer) Read(b []byte) (int, error) {
	select {
	case s.cb.sourceMutex <- struct{}{}:
		defer func() {
			<-s.cb.sourceMutex
		}()
		if !s.buf.Empty() {
			return s.buf.Read(b)
		}
		if s.cb.IsEOF {
			return 0, io.EOF
		}
		n, err := s.cb.source.Read(b)
		if n > 0 {
			if err := s.cb.Clone(s, b[0:n]); err != nil {
				return n, err
			}
		}
		if err == io.EOF {
			s.cb.EOF()
			return 0, io.EOF
		}
		return n, err
	}
}

func (cb *Demuxer) EOF() {
	cb.IsEOF = true
	for _, c := range cb.Clones {
		c.buf.EOF()
	}
}

func (cb *Demuxer) NewBuffer() *Buffer {
	s := &Buffer{}

	s.cb = cb
	s.buf = threaded.NewBuffer()

	cb.Clones = append(cb.Clones, s)
	return s
}

func (cb *Demuxer) Clone(s *Buffer, b []byte) error {
	for _, c := range cb.Clones {
		if s == c {
			continue
		}
		if _, err := c.buf.Write(b); err != nil {
			return err
		}
	}
	return nil
}

func New(s io.Reader) *Demuxer {
	cb := &Demuxer{}

	cb.sourceMutex = make(chan struct{}, 1)
	cb.source = s

	return cb
}
