package threaded

import (
	"bytes"
	"fmt"
	"io"
	"sync"
)

type Buffer struct {
	bufferMutex *sync.RWMutex
	buffer      *bytes.Buffer

	eof    bool
	closed bool

	Event chan struct{}
}

func (bf *Buffer) read(b []byte) (int, error) {
	bf.bufferMutex.RLock()
	defer bf.bufferMutex.RUnlock()

	n, err := bf.buffer.Read(b)
	if err == io.EOF {
		bf.buffer.Reset()
		return n, nil
	}

	if err != nil {
		return n, err
	}
	return n, nil
}

func (bf *Buffer) Close() error {
	bf.closed = true
	bf.EOF()
	return nil
}

func (bf *Buffer) Empty() bool {
	return bf.buffer.Len() == 0
}

func (bf *Buffer) EOF() {
	bf.eof = true
	bf.NewEvent()
}

func (bf *Buffer) Free() bool {
	return len(bf.Event) < cap(bf.Event)
}

func (bf *Buffer) NewEvent() {
	if bf.Free() {
		bf.Event <- struct{}{}
	}
}

func (bf *Buffer) Read(b []byte) (int, error) {
	if !bf.Empty() {
		return bf.read(b)
	}
	if bf.eof {
		return 0, io.EOF
	}
	select {
	case <-bf.Event:
		return bf.Read(b)
	}
}

func (bf *Buffer) Write(b []byte) (int, error) {
	if bf.closed {
		return 0, fmt.Errorf("Buffer is closed")
	}

	bf.bufferMutex.Lock()
	defer bf.bufferMutex.Unlock()

	n, err := bf.buffer.Write(b)
	if err != nil {
		return n, err
	}

	bf.NewEvent()
	return n, nil
}

func NewBuffer() *Buffer {
	b := &Buffer{}

	b.buffer = &bytes.Buffer{}
	b.bufferMutex = &sync.RWMutex{}

	b.Event = make(chan struct{}, 1)

	return b
}
