package sink

import (
	"gitlab.com/ilmikko/overwatch/overwatch/io/null"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
)

type Null struct {
	pipe.DrainBase
}

func NewNull() pipe.Sink {
	s := &Null{}

	s.AddWriter(&null.Writer{})

	return s
}
