package sink

import (
	"os"
	"sync"

	"gitlab.com/ilmikko/overwatch/overwatch/bufio/writer_closer"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
)

type File struct {
	pipe.DrainBase
}

func NewFile(file string) (pipe.Drain, error) {
	s := &File{}

	f, err := os.Create(file)
	if err != nil {
		return nil, err
	}

	s.AddWriter(writer_closer.New(f, &sync.Mutex{}))

	return s, nil
}
