package sink

import (
	"os/exec"
	"strings"
	"sync"

	"gitlab.com/ilmikko/overwatch/overwatch/bufio/writer_closer"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
)

type Fork struct {
	pipe.DrainBase

	cmd *exec.Cmd
}

func NewFork(cl string) (pipe.Drain, error) {
	s := &Fork{}

	fields := strings.Fields(cl)
	s.cmd = exec.Command(fields[0], fields[1:]...)

	ip, err := s.cmd.StdinPipe()
	if err != nil {
		return nil, err
	}
	if err := s.cmd.Start(); err != nil {
		return nil, err
	}

	s.AddWriter(writer_closer.New(ip, &sync.Mutex{}))

	return s, nil
}
