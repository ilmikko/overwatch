package sink

import (
	"os/exec"
	"strings"
	"sync"

	"gitlab.com/ilmikko/overwatch/overwatch/bufio/writer_closer"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Exec struct {
	pipe.DrainBase

	cl []string
}

func (s *Exec) Writer() (str.Writer, error) {
	cmd := exec.Command(s.cl[0], s.cl[1:]...)

	ip, err := cmd.StdinPipe()
	if err != nil {
		return nil, err
	}

	if err := cmd.Start(); err != nil {
		return nil, err
	}

	writer := writer_closer.New(ip, &sync.Mutex{})

	s.AddWriter(writer)

	return writer, nil
}

func NewExec(cl string) (pipe.Drain, error) {
	s := &Exec{}

	s.cl = strings.Fields(cl)

	return s, nil
}
