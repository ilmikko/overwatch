package sink

import (
	"os"
	"sync"

	"gitlab.com/ilmikko/overwatch/overwatch/bufio/writer_closer"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
)

type Stdout struct {
	pipe.DrainBase
}

func NewStdout() pipe.Drain {
	s := &Stdout{}

	s.AddWriter(writer_closer.New(os.Stdout, &sync.Mutex{}))

	return s
}
