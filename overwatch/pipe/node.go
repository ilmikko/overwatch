package pipe

import (
	"gitlab.com/ilmikko/overwatch/overwatch/bufio/demuxer"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
	"gitlab.com/ilmikko/overwatch/overwatch/unit"
)

type Node struct {
	Base

	clone *demuxer.Demuxer
}

func (p *Node) AddInflow(r str.Reader) {
	p.Inflows = append(p.Inflows, r)
	p.clone = demuxer.New(unit.NewMux(p.Inflows...))
}

func (p *Node) GetOutflow() str.Reader {
	return p.clone.NewBuffer()
}

func NewNode() (Pipe, error) {
	p := &Node{}

	return p, nil
}
