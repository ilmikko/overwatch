package pipe

import (
	"bufio"
	"bytes"
	"fmt"
	"os/exec"
	"strings"
	"sync"

	"gitlab.com/ilmikko/overwatch/overwatch/bufio/writer_closer"
	"gitlab.com/ilmikko/overwatch/overwatch/io/demuxer"
	"gitlab.com/ilmikko/overwatch/overwatch/io/threaded"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Fork struct {
	DrainBase
	Source

	cmderr      chan error
	cmd         *exec.Cmd
	stdoutClone *demuxer.Demuxer
}

func (p *Fork) Close() error {
	defer p.stdoutClone.EOF()
	for _, w := range p.writers {
		if err := w.Close(); err != nil {
			return err
		}
	}
	if err := <-p.cmderr; err != nil {
		return err
	}
	return nil
}

func (p *Fork) GetOutflow() str.Reader {
	return bufio.NewReader(p.stdoutClone.NewBuffer())
}

func NewFork(cl string) (*Fork, error) {
	p := &Fork{}

	p.cmderr = make(chan error, 1)

	fields := strings.Fields(cl)
	p.cmd = exec.Command(fields[0], fields[1:]...)

	stderr := &bytes.Buffer{}
	p.cmd.Stderr = stderr

	stdout := threaded.NewBuffer()
	p.cmd.Stdout = stdout
	p.stdoutClone = demuxer.New(stdout)

	ip, err := p.cmd.StdinPipe()
	if err != nil {
		return nil, err
	}
	if err := p.cmd.Start(); err != nil {
		return nil, err
	}
	go func() {
		if err := p.cmd.Wait(); err != nil {
			p.cmderr <- fmt.Errorf("%v: %v", err, stderr.String())
			return
		}
		p.cmderr <- nil
	}()

	p.AddWriter(writer_closer.New(ip, &sync.Mutex{}))

	return p, nil
}
