package pipe

import (
	"io"
	"log"
	"sync"

	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Pipe interface {
	Sink
	Source
}

type Sink interface {
	AddInflow(str.Reader)
	Drain(str.LockWriter) error
}

type Source interface {
	GetOutflow() str.Reader
}

type Base struct {
	Inflows []str.Reader
}

func (p *Base) AddInflow(i str.Reader) {
	p.Inflows = append(p.Inflows, i)
}

func (p *Base) Drain(w str.LockWriter) error {
	wg := sync.WaitGroup{}
	wg.Add(len(p.Inflows))
	for _, i := range p.Inflows {
		go func(r str.Reader) {
			defer wg.Done()
			if err := p.drainFlow(r, w); err != nil {
				log.Fatalf("FLOW DRAIN: %v", err)
			}
		}(i)
	}
	wg.Wait()
	return nil
}

func (p *Base) drainFlow(r str.Reader, w str.LockWriter) error {
	for {
		s, err := r.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		if err := p.writeLine(w, s); err != nil {
			return err
		}
	}
	return nil
}

func (p *Base) writeLine(w str.LockWriter, s string) error {
	w.Lock()
	defer w.Unlock()
	if _, err := w.WriteString(s); err != nil {
		return err
	}
	if err := w.Flush(); err != nil {
		return err
	}
	return nil
}
