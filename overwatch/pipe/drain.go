package pipe

import (
	"log"
	"sync"

	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Drain interface {
	Sink

	AddWriter(str.LockWriter)
	Close() error
	Flow() error
}

type DrainBase struct {
	Base

	writers []str.LockWriter
}

func (d *DrainBase) AddWriter(w str.LockWriter) {
	d.writers = append(d.writers, w)
}

func (d *DrainBase) Close() error {
	for _, w := range d.writers {
		if err := w.Close(); err != nil {
			return err
		}
	}
	return nil
}

func (d *DrainBase) Flow() error {
	wg := sync.WaitGroup{}
	wg.Add(len(d.writers))
	for _, w := range d.writers {
		go func(w str.LockWriter) {
			defer wg.Done()
			if err := d.Drain(w); err != nil {
				log.Printf("DRAIN: %v", err)
			}
		}(w)
	}
	wg.Wait()
	return nil
}
