package filter

import (
	"regexp"
)

type StringManipulator interface {
	MatchString(string) bool
	ReplaceAllString(a, b string) string
}

type Filter struct {
	Id string
	or []StringManipulator
}

func (f *Filter) Regexp(s string) error {
	r, err := regexp.Compile(s)
	if err != nil {
		return err
	}

	f.or = append(f.or, r)
	return nil
}

func (f *Filter) Matches(s string) bool {
	for _, m := range f.or {
		if m.MatchString(s) {
			return true
		}
	}
	return false
}

func (f *Filter) Replace(s, r string) string {
	for _, m := range f.or {
		s = m.ReplaceAllString(s, r)
	}
	return s
}

func New(id string) *Filter {
	f := &Filter{}

	f.Id = id
	f.or = []StringManipulator{}

	return f
}
