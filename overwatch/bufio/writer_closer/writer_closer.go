package writer_closer

import (
	"bufio"
	"io"
	"sync"
)

type WriterCloser struct {
	*bufio.Writer
	original io.WriteCloser
	mutex    *sync.Mutex
}

func (w *WriterCloser) Close() error {
	if err := w.Flush(); err != nil {
		return err
	}
	if err := w.original.Close(); err != nil {
		return err
	}
	return nil
}

func (w *WriterCloser) Lock() {
	w.mutex.Lock()
}

func (w *WriterCloser) Unlock() {
	w.mutex.Unlock()
}

func New(ow io.WriteCloser, m *sync.Mutex) *WriterCloser {
	return &WriterCloser{bufio.NewWriter(ow), ow, m}
}
