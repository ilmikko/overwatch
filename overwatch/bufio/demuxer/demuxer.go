package demuxer

import (
	"io"

	"gitlab.com/ilmikko/overwatch/overwatch/io/threaded"
	"gitlab.com/ilmikko/overwatch/overwatch/str"
)

type Demuxer struct {
	Clones []*Buffer
	IsEOF  bool

	sourceMutex chan struct{}
	source      str.Reader
}

type Buffer struct {
	cb  *Demuxer
	buf *str.EOFBuffer
}

func (s *Buffer) ReadString(b byte) (string, error) {
	select {
	case s.cb.sourceMutex <- struct{}{}:
		defer func() {
			<-s.cb.sourceMutex
		}()
		if !s.buf.Empty() {
			return s.buf.ReadString(b)
		}
		if s.cb.IsEOF {
			return "", io.EOF
		}
		str, err := s.cb.source.ReadString(b)
		if err != nil {
			if err == io.EOF {
				s.cb.EOF()
				return str, io.EOF
			}
			return str, err
		}
		if err := s.cb.Clone(s, str); err != nil {
			return str, err
		}
		return str, nil
	}
}

func (cb *Demuxer) EOF() {
	cb.IsEOF = true
	for _, c := range cb.Clones {
		c.buf.EOF()
	}
}

func (cb *Demuxer) NewBuffer() *Buffer {
	s := &Buffer{}

	s.cb = cb
	s.buf = str.NewEOFBuffer(threaded.NewBuffer())

	cb.Clones = append(cb.Clones, s)
	return s
}

func (cb *Demuxer) Clone(b *Buffer, s string) error {
	for _, c := range cb.Clones {
		if b == c {
			continue
		}
		if _, err := c.buf.WriteString(s); err != nil {
			return err
		}
		if err := c.buf.Flush(); err != nil {
			return err
		}
	}
	return nil
}

func New(s str.Reader) *Demuxer {
	cb := &Demuxer{}

	cb.sourceMutex = make(chan struct{}, 1)
	cb.source = s

	return cb
}
