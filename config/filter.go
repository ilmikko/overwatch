package config

import (
	"fmt"

	"gitlab.com/ilmikko/overwatch/overwatch/filter"
)

func (cfg *Config) getFilter(id string) (*filter.Filter, error) {
	f, ok := cfg.filters[id]
	if !ok {
		return nil, fmt.Errorf("INVALID FILTER: %q", id)
	}
	return f, nil
}
