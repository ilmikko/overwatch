package config

import (
	"gitlab.com/ilmikko/overwatch/comm"
	"gitlab.com/ilmikko/overwatch/config/parser"
	"gitlab.com/ilmikko/overwatch/overwatch/filter"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
)

type Config struct {
	Comm   *comm.Comm
	Drains []pipe.Drain

	parser  *parser.Parser
	filters map[string]*filter.Filter
	pipes   map[string]pipe.Pipe
	sources map[string]pipe.Source
	sinks   map[string]pipe.Sink
}

func Load(co *comm.Comm) (*Config, error) {
	cfg := New()

	cfg.Comm = co
	if err := cfg.flagLoad(); err != nil {
		return nil, err
	}
	cfg.Comm.Print(comm.INIT_CFG_DONE)
	return cfg, nil
}

func New() *Config {
	cfg := &Config{}

	cfg.Comm = comm.New()
	cfg.Drains = []pipe.Drain{}

	cfg.parser = parser.New()

	cfg.filters = map[string]*filter.Filter{}

	cfg.pipes = map[string]pipe.Pipe{}
	cfg.sinks = map[string]pipe.Sink{}
	cfg.sources = map[string]pipe.Source{}

	return cfg
}

func init() {
	flagInit()
}
