package config

import (
	"fmt"

	"gitlab.com/ilmikko/overwatch/config/parser"
	"gitlab.com/ilmikko/overwatch/overwatch/filter"
	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
	"gitlab.com/ilmikko/overwatch/overwatch/sink"
	"gitlab.com/ilmikko/overwatch/overwatch/source"
	"gitlab.com/ilmikko/overwatch/overwatch/unit"
)

func (cfg *Config) pConfig(fields []string) error {
	l := len(fields)
	file, context := fields[l-1], fields[:l-1]
	if err := cfg.Load(file, context...); err != nil {
		return fmt.Errorf("DIRECTIVE %q: %v", fields, err)
	}
	return nil
}

func (cfg *Config) pFilterCreate(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.filters[id] = filter.New(id)
	return nil
}

func (cfg *Config) pFilterRegexp(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	re, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	f, err := cfg.getFilter(id)
	if err != nil {
		return err
	}

	if err := f.Regexp(re); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pFilter(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"CREATE": cfg.pFilterCreate,
		"REGEXP": cfg.pFilterRegexp,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pStreamCopy(fields []string) error {
	in, out, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	i, err := cfg.getSource(in)
	if err != nil {
		return err
	}

	o, err := cfg.getSink(out)
	if err != nil {
		return err
	}

	o.AddInflow(i.GetOutflow())
	return nil
}

func (cfg *Config) pStreamCount(fields []string) error {
	in, out, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	i, err := cfg.getSource(in)
	if err != nil {
		return err
	}

	o, err := cfg.getSink(out)
	if err != nil {
		return err
	}

	o.AddInflow(unit.NewCount(i.GetOutflow()))
	return nil
}

func (cfg *Config) pStreamLimit(fields []string) error {
	in, sn, out, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	n, err := cfg.parser.IntPositive(sn)
	if err != nil {
		return err
	}

	i, err := cfg.getSource(in)
	if err != nil {
		return err
	}

	o, err := cfg.getSink(out)
	if err != nil {
		return err
	}

	o.AddInflow(unit.NewLimit(n, i.GetOutflow()))
	return nil
}

func (cfg *Config) pStreamMatch(fields []string) error {
	in, filter, out, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	i, err := cfg.getSource(in)
	if err != nil {
		return err
	}

	f, err := cfg.getFilter(filter)
	if err != nil {
		return err
	}

	o, err := cfg.getSink(out)
	if err != nil {
		return err
	}

	o.AddInflow(unit.NewMatch(f, i.GetOutflow()))
	return nil
}

func (cfg *Config) pStreamNotLimit(fields []string) error {
	in, sn, out, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	n, err := cfg.parser.IntPositive(sn)
	if err != nil {
		return err
	}

	i, err := cfg.getSource(in)
	if err != nil {
		return err
	}

	o, err := cfg.getSink(out)
	if err != nil {
		return err
	}

	o.AddInflow(unit.NewLimitNegated(n, i.GetOutflow()))
	return nil
}

func (cfg *Config) pStreamNotMatch(fields []string) error {
	in, filter, out, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	i, err := cfg.getSource(in)
	if err != nil {
		return err
	}

	f, err := cfg.getFilter(filter)
	if err != nil {
		return err
	}

	o, err := cfg.getSink(out)
	if err != nil {
		return err
	}

	o.AddInflow(unit.NewMatchNegated(f, i.GetOutflow()))
	return nil
}

func (cfg *Config) pStreamNot(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"LIMIT": cfg.pStreamNotLimit,
		"MATCH": cfg.pStreamNotMatch,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pStreamReplace(fields []string) error {
	in, filter, out, err := cfg.parser.Grab3(fields)
	if err != nil {
		return err
	}

	fields, err = cfg.parser.ShiftN(fields, 3)
	if err != nil {
		return err
	}

	replacement, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	i, err := cfg.getSource(in)
	if err != nil {
		return err
	}

	f, err := cfg.getFilter(filter)
	if err != nil {
		return err
	}

	o, err := cfg.getSink(out)
	if err != nil {
		return err
	}

	o.AddInflow(unit.NewReplace(f, replacement, i.GetOutflow()))
	return nil
}

func (cfg *Config) pStream(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"COPY":    cfg.pStreamCopy,
		"COUNT":   cfg.pStreamCount,
		"LIMIT":   cfg.pStreamLimit,
		"MATCH":   cfg.pStreamMatch,
		"NOT":     cfg.pStreamNot,
		"REPLACE": cfg.pStreamReplace,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pUnitPipeFork(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	cl, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	p, err := pipe.NewFork(cl)
	if err != nil {
		return err
	}
	cfg.pipes[id] = p
	cfg.Drains = append(cfg.Drains, p)
	return nil
}

func (cfg *Config) pUnitPipeNode(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	p, err := pipe.NewNode()
	if err != nil {
		return err
	}

	cfg.pipes[id] = p
	return nil
}

func (cfg *Config) pUnitPipe(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"FORK": cfg.pUnitPipeFork,
		"NODE": cfg.pUnitPipeNode,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pUnitSinkFork(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	cl, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	s, err := sink.NewFork(cl)
	if err != nil {
		return err
	}
	cfg.sinks[id] = s
	cfg.Drains = append(cfg.Drains, s)
	return nil
}

func (cfg *Config) pUnitSinkNull(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	s := sink.NewNull()
	cfg.sinks[id] = s
	return nil
}

func (cfg *Config) pUnitSinkStdout(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	s := sink.NewStdout()
	cfg.sinks[id] = s
	cfg.Drains = append(cfg.Drains, s)
	return nil
}

func (cfg *Config) pUnitSinkWrite(fields []string) error {
	id, file, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	s, err := sink.NewFile(file)
	if err != nil {
		return err
	}
	cfg.sinks[id] = s
	cfg.Drains = append(cfg.Drains, s)
	return nil
}

func (cfg *Config) pUnitSink(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"FORK":   cfg.pUnitSinkFork,
		"NULL":   cfg.pUnitSinkNull,
		"STDOUT": cfg.pUnitSinkStdout,
		"WRITE":  cfg.pUnitSinkWrite,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pUnitSourceExec(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	cmdline, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	s, err := source.NewExec(cmdline)
	if err != nil {
		return err
	}
	cfg.sources[id] = s
	return nil
}

func (cfg *Config) pUnitSourceFork(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	cmdline, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	s, err := source.NewFork(cmdline)
	if err != nil {
		return err
	}
	cfg.sources[id] = s
	return nil
}

func (cfg *Config) pUnitSourceNull(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.sources[id] = source.NewNull()
	return nil
}

func (cfg *Config) pUnitSourceRead(fields []string) error {
	id, file, err := cfg.parser.Grab2(fields)
	if err != nil {
		return err
	}

	s, err := source.NewFile(file)
	if err != nil {
		return err
	}
	cfg.sources[id] = s
	return nil
}

func (cfg *Config) pUnitSourceStdin(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.sources[id] = source.NewStdin()
	return nil
}

func (cfg *Config) pUnitSourceWait(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.sources[id] = source.NewWait()
	return nil
}

func (cfg *Config) pUnitSource(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"EXEC":  cfg.pUnitSourceExec,
		"FORK":  cfg.pUnitSourceFork,
		"NULL":  cfg.pUnitSourceNull,
		"READ":  cfg.pUnitSourceRead,
		"STDIN": cfg.pUnitSourceStdin,
		"WAIT":  cfg.pUnitSourceWait,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pUnit(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"PIPE":   cfg.pUnitPipe,
		"SINK":   cfg.pUnitSink,
		"SOURCE": cfg.pUnitSource,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) p(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"CONFIG": cfg.pConfig,
		"FILTER": cfg.pFilter,
		"STREAM": cfg.pStream,
		"UNIT":   cfg.pUnit,
	}); err != nil {
		return err
	}
	return nil
}
