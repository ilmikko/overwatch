package config

import (
	"fmt"

	"gitlab.com/ilmikko/overwatch/overwatch/pipe"
)

func (cfg *Config) getPipe(id string) (pipe.Pipe, error) {
	u, ok := cfg.pipes[id]
	if !ok {
		return nil, fmt.Errorf("INVALID PIPE: %q.", id)
	}
	return u, nil
}

func (cfg *Config) getSink(id string) (pipe.Sink, error) {
	if s, err := cfg.getPipe(id); err == nil {
		return s, nil
	}
	u, ok := cfg.sinks[id]
	if !ok {
		return nil, fmt.Errorf("INVALID SINK: %q.", id)
	}
	return u, nil
}

func (cfg *Config) getSource(id string) (pipe.Source, error) {
	if s, err := cfg.getPipe(id); err == nil {
		return s, nil
	}
	u, ok := cfg.sources[id]
	if !ok {
		return nil, fmt.Errorf("INVALID SOURCE: %q.", id)
	}
	return u, nil
}
