package parser_test

import (
	"fmt"
	"testing"

	"gitlab.com/ilmikko/lfs/config/parser"
)

func TestGrab1(t *testing.T) {
	p := parser.New()

	testCases := []struct {
		fields  []string
		want    string
		wantErr error
	}{
		{
			fields: []string{"COMMAND"},
			want:   "COMMAND",
		},
		{
			fields: []string{"COMMAND", "COMMAND2"},
			want:   "COMMAND",
		},
		{
			fields:  []string{},
			wantErr: fmt.Errorf("needs additional fields."),
		},
	}

	for _, tc := range testCases {
		got, err := p.Grab1(tc.fields)
		want := tc.want
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error %q but was nil!", tc.wantErr.Error())
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("GrabAll: %v", err)
			}
		}
		if got != want {
			t.Errorf("GrabAll string not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestGrabAll(t *testing.T) {
	p := parser.New()

	testCases := []struct {
		fields  []string
		want    string
		wantErr error
	}{
		{
			fields: []string{"COMMAND"},
			want:   "COMMAND",
		},
		{
			fields: []string{"COMMAND", "COMMAND2"},
			want:   "COMMAND COMMAND2",
		},
		{
			fields:  []string{},
			wantErr: fmt.Errorf("needs additional fields."),
		},
	}

	for _, tc := range testCases {
		got, err := p.GrabAll(tc.fields)
		want := tc.want
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error %q but was nil!", tc.wantErr.Error())
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("GrabAll: %v", err)
			}
		}
		if got != want {
			t.Errorf("GrabAll string not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestLeftN(t *testing.T) {
	p := parser.New()

	testCases := []struct {
		fields []string
		number int
		want   error
	}{
		{
			fields: []string{"COMMAND"},
			number: 1,
			want:   nil,
		},
		{
			fields: []string{"COMMAND"},
			number: 2,
			want:   fmt.Errorf("needs additional fields (1 supplied, 2 required)."),
		},
		{
			fields: []string{"COMMAND", "COMMAND2"},
			number: 2,
			want:   nil,
		},
		{
			fields: []string{},
			number: 2,
			want:   fmt.Errorf("needs additional fields (0 supplied, 2 required)."),
		},
		{
			fields: []string{},
			number: 1,
			want:   fmt.Errorf("needs additional fields (0 supplied, 1 required)."),
		},
	}

	for _, tc := range testCases {
		got := p.LeftN(tc.fields, tc.number)
		want := tc.want
		if got == nil {
			got = fmt.Errorf("")
		}
		if want == nil {
			want = fmt.Errorf("")
		}
		{
			got := got.Error()
			want := want.Error()
			if got != want {
				t.Errorf("Left error not as expected: \n got: %q\nwant: %q", got, want)
			}
		}
	}
}

func TestLeft(t *testing.T) {
	p := parser.New()

	testCases := []struct {
		fields []string
		want   error
	}{
		{
			fields: []string{"COMMAND"},
			want:   nil,
		},
		{
			fields: []string{""},
			want:   nil,
		},
		{
			fields: []string{},
			want:   fmt.Errorf("needs additional fields."),
		},
	}

	for _, tc := range testCases {
		got := p.Left(tc.fields)
		want := tc.want
		if got == nil {
			got = fmt.Errorf("")
		}
		if want == nil {
			want = fmt.Errorf("")
		}
		{
			got := got.Error()
			want := want.Error()
			if got != want {
				t.Errorf("Left error not as expected: \n got: %q\nwant: %q", got, want)
			}
		}
	}
}
