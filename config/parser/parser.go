package parser

import (
	"fmt"
	"strings"
)

type Parser struct{}

func (p *Parser) Unknown(cmd string, help ...string) error {
	if len(help) == 0 {
		return fmt.Errorf("UNKNOWN DIRECTIVE: %q.", cmd)
	}

	return fmt.Errorf("UNKNOWN DIRECTIVE: %q. %s.", cmd, strings.Join(help, "\n"))
}

func New() *Parser {
	p := &Parser{}

	return p
}
