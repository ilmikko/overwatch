package parser_test

import (
	"fmt"
	"testing"

	"gitlab.com/ilmikko/lfs/config/parser"
)

func TestIsRule(t *testing.T) {
	p := parser.New()

	testCases := []struct {
		line string
		want bool
	}{
		{
			line: "COMMAND",
			want: true,
		},
		{
			line: "MULTI LINE COMMAND",
			want: true,
		},
		{
			line: "",
			want: false,
		},
		{
			line: "       ",
			want: false,
		},
		{
			line: "# COMMENT",
			want: false,
		},
		{
			line: "#COMMENT",
			want: false,
		},
		{
			line: "#COMMENT WITH SPACES",
			want: false,
		},
		{
			line: "###########",
			want: false,
		},
		{
			line: "    # COMMENT",
			want: false,
		},
		{
			line: "COMMAND # NOT A COMMENT",
			want: true,
		},
	}

	for _, tc := range testCases {
		got := p.IsRule(tc.line)
		want := tc.want

		if got != want {
			t.Errorf("Rule output for %q not as expected: \n got: %v\nwant: %v", tc.line, got, want)
		}
	}
}

func TestUnknown(t *testing.T) {
	p := parser.New()

	testCases := []struct {
		cmd  string
		help []string
		want error
	}{
		{
			cmd:  "COMMAND",
			want: fmt.Errorf("Unknown field: %q", "COMMAND"),
		},
		{
			cmd: "CMDHELP",
			help: []string{
				"Help string 1",
				"Help string 2",
			},
			want: fmt.Errorf("Unknown field: %q (%s\n%s)", "CMDHELP", "Help string 1", "Help string 2"),
		},
	}

	for _, tc := range testCases {
		got := p.Unknown(tc.cmd, tc.help...).Error()
		want := tc.want.Error()
		if got != want {
			t.Errorf("Unknown error not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}
