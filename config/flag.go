package config

import "flag"

var (
	flagConfigFile = flag.String("config", "", "OVERWATCH CONFIGURATION DIRECTIVE.")
	flagShowTime   = flag.Bool("time", true, "TIMESTAMP DISPLAY DIRECTIVE.")
)

func (cfg *Config) flagLoad() error {
	if f := *flagConfigFile; f != "" {
		if err := cfg.Load(f); err != nil {
			return err
		}
	}
	cfg.Comm.ShowTime = *flagShowTime
	return nil
}

func flagInit() {
	flag.Parse()
}
